<?php

class TransactionsController extends \AdminController {


	public $trans;
	/**
	 * Display a listing of the resource.
	 * GET /transactions
	 *
	 * @return Response
	 */
	public function index()
	{
		$raw = [];
		$keys = [];
		$agents = Agent::with('person')->get();
		$this->trans['agents'] = ( !empty($agents) )? $agents->toArray() : [];
		$td = DailyTransaction::with('agent.person')->get();
		// $payments = DailyTransaction::with('agent.person')->get();td

		$this->trans['payments']  = ( !empty($payments) )? $payments->toArray() : [];
		// $tes = Agent::with('dtransactions')->get();
		// dd( $tes->toArray() );
		// dd($td->toArray());
		// get rent payments
		// $rents = Rent::with('payments')->get();
		// // dd($rent->toArray());
		// // get land payments
		// // $tr = Transaction::with('payments')->get();
		// $payments = Payment::with('customer')->get();
		// $transactions = Transaction::with('customer')->get();

		// // dd($tr);
		// // get
		foreach ($td as $datr => $daily) {
			if (!empty($daily)) {
				$dayl = $daily->toArray();
				$raw[$dayl['date_paid']][] = $dayl;
			}		 	
		 }

		// if (!empty($rents)) {
		//  	foreach ($rents as $rr => $rent) {
		//  		if (!empty($rent)) {
		//  			$ren = $rent->toArray();
		//  			$see = head(preg_split('/[\s]/',$ren['rent_firstmonthpaid']));
		//  			$raw[$see][] = $ren;
		//  		}
		//  	}
		//  } 
		 foreach ($raw as $key => $value) {
		 	$keys[] = $key;
		 	asort($keys);
		 }
		 // var_dump(min($keys));
		 $this->trans['keys'] = $keys;
		 $this->trans['raw'] = $raw;
		// die();
		$this->layout->content = View::make('admin.transactions.index')->with('trans',$this->trans);
	}

	/**
	 * Show the form for creating a new resource.
	 * GET /transactions/create
	 *
	 * @return Response
	 */
	public function create()
	{
		//
	}

	/**
	 * Store a newly created resource in storage.
	 * POST /transactions
	 *
	 * @return Response
	 */
	public function store()
	{
		#notice no validation

	}

	/**
	 * Display the specified resource.
	 * GET /transactions/{id}
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show($id = 1)
	{
			$all = Tenant::with('house.compound','person.contacts','person.addresses','rents.payments','person.documents')->whereRaw('tent_id = ? AND deleted = ?',[$id,0])->first();
		//$all = Tenant::with('house.compound','person.contacts','person.addresses','rents.payments')->whereRaw('tent_id = ? AND deleted = ? AND tent_status = ?',[$id,0,0])->first();
		$all = ($all)? $all->toArray() : [];
		// dd($all);
		$this->layout->content = View::make('admin.transactions.show')->with('tenant',$all);
	}

	/**
	 * Show the form for editing the specified resource.
	 * GET /transactions/{id}/edit
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit($id)
	{
		//
	}

	/**
	 * Update the specified resource in storage.
	 * PUT /transactions/{id}
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update($id)
	{
		//
	}

	/**
	 * Remove the specified resource from storage.
	 * DELETE /transactions/{id}
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($id)
	{
		//
	}
	public function getCheckdate(){
		$raw = [];
		$keys = [];
		$results = [];
		$input = Input::all();
		if (Input::has('type') && Input::get('type') == 'adate') {




			// vet the date
			// $date = Input::get('year').'-'.Input::get('month').'-'.Input::get('day');
			// var_dump($input);

				$year = (empty($input['year']) || $input['year'] < 1500)? date('Y') : $input['year'];

				$month = (empty($input['month']) || $input['month'] > 12)? date('m') : $input['month'];

				$day = (empty($input['day']) || $input['day'] > 31)? date('j') : $input['day'];

				$date = $year.'-'.$month.'-'.$day;


			if(isset($input['day']) && !empty($input['day']) && !empty($input['month']) && !empty($input['year'])):

				

				// $trans = DailyTransaction::all();
				$trans = DailyTransaction::with('agent.person')->whereRaw('date_paid = ?',[$date])->get();

					if(!empty($trans)):

							foreach ($trans as $key => $value) {
								
								if(!empty($value)){
									$dd = $value->toArray();
									$results[$dd['date_paid']][] = $dd;
								}

							}
							// die(var_dump($results));
					endif;
			else:

				// die(var_dump('expression'));

				$year = (empty($input['year']) || $input['year'] < 1500)? date('Y') : $input['year'];

				$month = (empty($input['month']) || $input['month'] > 12)? date('m') : $input['month'];

				$day = (empty($input['day']) || $input['day'] > 31)? date('j') : $input['day'];

				$date = $year.'-'.$month.'-'.$day;

			// 	// $trans = DailyTransaction::with('agent')->whereRaw('date_paid = ? || created_at = ?',[$date,$date])->get();
			// 	// construct the date
			// 	// query the date
				$raw = DB::select(DB::raw("
							( SELECT
								EXTRACT( YEAR_MONTH FROM date_paid) , id
								FROM daily_transactions
								WHERE MATCH(date_paid)
								AGAINST('$date' IN BOOLEAN MODE)
							)"));

						if ($raw):
							$check = [];
							foreach ($raw as $key => $value) {
								$trans = DailyTransaction::find($value->id)->with('agent.person')->first();
									if(!empty($trans)):
									// var_dump( $trans->toArray() );
										$dd = $trans->toArray();
										$results[$dd['date_paid']][] = $dd;
										// var_dump($dd);
									endif;
							}
						endif;

				// die(var_dump($results));		
			endif;

// die(var_dump($results));
			// endif;
			// check and see if the date is not in the future
			// check the date format and make it the same format like the mysql database format
			// check and see if the date is either only year or month;




			
		}else{
				$yearf = (empty($input['from']['year']) || $input['from']['year'] < 1500)? date('Y') : $input['from']['year'];
				$monthf = (empty($input['from']['month']) || $input['from']['month'] > 12 )? date('m') : $input['from']['month'];
				$dayf = (empty($input['from']['day']) || $input['from']['day'] > 31)? date('j') : $input['from']['day'];
				$from = $yearf.'-'.$monthf.'-'.$dayf;


				$yeart = (empty($input['to']['year']) || $input['to']['year'] < 1500)? date('Y') : $input['to']['year'];
				$montht = (empty($input['to']['month']) || $input['to']['month'] > 12)? date('m') : $input['to']['month'];
				$dayt = (empty($input['to']['day']) || $input['to']['day'] > 31)? date('j') : $input['to']['day'];
				$to = $yeart.'-'.$montht.'-'.$dayt;
			// die(var_dump( Input::all() ));

			// check and see if the day is set
			// if (isset($input['from']['day'])) {
			// 	# code...
			// }
			// check and see if month only is set
			// check and see if mon and year is set
			// check and see if only years are yet
			$trans = DailyTransaction::with('agent.person')->whereRaw('date_paid BETWEEN ?  AND ? ',[$from,$to])->get();
			foreach ($trans as $key => $value) {
				
				if(!empty($value)){
					$dd = $value->toArray();
					$results[$dd['date_paid']][] = $dd;
				}

			}

		}
		if (!empty($results)) {
			 foreach ($results as $key => $value) {
			 	$keys[] = $key;
			 	asort($keys);
			 }			
		}

		 // var_dump(min($keys));
		 $this->trans['keys'] = $keys;
		 $this->trans['results'] = $results;


// dd($this->trans);

		// $this->layout->content = View::make('admin.transactions.index')->with('trans',$this->trans);
		$this->layout->content = View::make('admin.transactions.show')->with('trans',$this->trans);
	}

}