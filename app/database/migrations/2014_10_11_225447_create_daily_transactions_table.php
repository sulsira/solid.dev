<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateDailyTransactionsTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('daily_transactions', function(Blueprint $table)
		{
			$table->increments('id');
			$table->integer('entityID')->default(0);
			$table->string('entityType')->nullable();
			$table->integer('userID')->default(0);
			$table->string('type')->nullable();
			$table->string('paid_for')->nullable();
			$table->string('customer')->nullable();
			$table->string('agentID')->nullable();
			$table->string('date_paid')->nullable();
			$table->string('locationOrProperty')->nullable();
			$table->string('description')->nullable();
			$table->string('ref')->nullable();
			$table->decimal('amount',20,3)->nullable();
			$table->timestamps();
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('daily_transactions');
	}

}
