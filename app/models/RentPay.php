<?php
use Laracasts\Commander\Events\EventGenerator;
use Universal\Creation\Events\TenantCreated;
class RentPay extends \Eloquent {
	use EventGenerator;
	protected $table = 'RentPays';
	protected $primaryKey = 'id';
	protected $fillable = ['tenant_id',
'll_id',
'compound_id',
'year',
'jan',
'feb',
'mar',
'apr',
'may',
'jun',
'jul',
'aug',
'sep',
'oct',
'nov',
'dec',
'status',
'deleted',
'amount_paid'
];

public function tenant(){
	return $this->belongsTo('Tenant');
}
public function landlord(){
	return $this->belongsTo('Landlord');
}
public static function creation($input){

}
}