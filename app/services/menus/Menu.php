<?php namespace services\menus;

abstract class Menu{
	protected $user_data = array();
	public $raw = array();
	public $incoming;
	public $menu = array();
	public $done_menu;
	public  $default = [
			[
			'name'=>'home',
			'visible'=> 1 ,
			'url'=>'/admin',
			'domains'=>'all',
			'type'=>'single',
			'security'=>[
						'level'=> 1 ,
						'views'=> 'all'

						]
			],
			[
			'name'=>'land lords',
			'visible'=> 1 ,
			'url'=>'/land-lords',
			'domains'=>'users',
			'type'=>'single',
			'security'=>[
						'level'=> 1 ,
						'views'=> 'all'
						]
			 ],
			[
			'name'=>'compounds',
			'visible'=> 1 ,
			'url'=>'/compounds',
			'domains'=>'rent',
			'type'=>'single',
			'security'=>[
						'level'=> 3 ,
						'views'=> 'rent|admin|access'
						]
			 ],
			[
			'name'=>'houses',
			'visible'=> 1 ,
			'url'=>'/houses',
			'domains'=>'rent',
			'type'=>'single',
			'security'=>[
						'level'=> 5 ,
						'views'=> 'rent|admin|access'
						]
			 ],
			[
			'name'=>'notifications',
			'visible'=> 0 ,
			'url'=>'/notifications',
			'domains'=>'all',
			'type'=>'single',
			'security'=>[
						'level'=> 8 ,
						'views'=> 'all|admin|access'
						]
			 ],
			[
			'name'=>'Payments',
			'visible'=> 0 ,
			'url'=>'/notifications',
			'domains'=>'admin',
			'type'=>'single',
			'security'=>[
						'level'=> 1 ,
						'views'=> 'all|admin|access'
						]
			 ],
			[
			'name'=>'transactions',
			'visible'=> 1 ,
			'url'=>'transactions',
			'domains'=>'admin',
			'type'=>'single',
			'security'=>[
					'level'=> 8,
					'views'=> 'admin|access'
					]
			],
			[
			'name'=>'Estates',
			'visible'=> 1 ,
			'url'=>'/estates',
			'domains'=>'agent',
			'type'=>'single',
			'security'=>[
						'level'=> 8,
						'views'=> 'agent|admin|access'
						]
			 ],
			[
			'name'=>'agents',
			'visible'=> 1 ,
			'url'=>'/agents',
			'domains'=>'agent',
			'type'=>'single',
			'security'=>[
						'level'=> 1 ,
						'views'=> 'agent|admin|access'
						]
			 ],
			[
			'name'=>'staff',
			'visible'=> 1 ,
			'url'=>'/staffs',
			'domains'=>'admin',
			'type'=>'single',
			'security'=>[
						'level'=> 1 ,
						'views'=> 'admin|access'
						]
			 ],
			[
			'name'=>'customers',
			'visible'=> 1 ,
			'url'=>'/customers',
			'domains'=>'agent',
			'type'=>'single',
			'security'=>[
						'level'=> 1 ,
						'views'=> 'agent|admin|access'
						]
			 ],
			[
			'name'=>'partners',
			'visible'=> 0 ,
			'url'=>'/partners',
			'domains'=>'agent',
			'type'=>'single',
			'security'=>[
						'level'=> 1 ,
						'views'=> 'agent|admin|access'
						]
			 ],
			[
			'name'=>'transactions',
			'visible'=> 0 ,
			'url'=>'/transactions',
			'domains'=>'admin',
			'type'=>'single',
			'security'=>[
						'level'=> 8 ,
						'views'=> 'admin|access'
						]
			 ],
			[
			'name'=>'Prospectives',
			'visible'=> 0 ,
			'url'=>'/prospectives',
			'domains'=>'admin',
			'type'=>'single',
			'security'=>[
						'level'=> 5 ,
						'views'=> 'agent|admin|access'
						]
			 ],
			[
			'name'=>'Tenants',
			'visible'=> 1 ,
			'url'=>'/tenants',
			'domains'=>'rent',
			'type'=>'single',
			'security'=>[
						'level'=> 5 ,
						'views'=> 'agent|admin|access'
						]
			 ],
			[
			'name'=>'Users',
			'visible'=> 1 ,
			'url'=>'/users',
			'domains'=>'admin',
			'type'=>'single',
			'security'=>[
						'level'=> 7 ,
						'views'=> 'admin|access'
						]
			 ],
			[
			'name'=>'--username--',
			'visible'=> 1 , 'url'=>'#','domains'=>'--self--','type'=>'drop', 'menu'=>[
				['name'=>'help','visible'=> 0 , 'url'=>'help','domains'=>'veda','type'=>'single','security'=>'8'],
				['name'=>'settings','visible'=> 1 , 'url'=>'admin/settings','domains'=>'veda','type'=>'single','security'=>'8'],
				['name'=>'logout','visible'=> 1 , 'url'=>'logout','domains'=>'veda','type'=>'single','security'=>'8']
			]]
		];
	// it would would try pick up what session you are on and your previledges
	// displays the pages you need to see by domain and clearance level
	// algorythm
		/**
		 * methods:
		 *        contruct incase a menu array is sent
		 *  /	 method( ) menu that return exactly what the view should see
		 *
		 * @return Response {}
		 */
		private function processMenu(){

			$session  = \Session::all();
			$menu = array();
			$disabled = array();

			// we have to make sure that the session is not empty 
				// if its empty we return the whole or we just logout the user

			if (!empty($session)):

				if ( !empty($this->incoming) ):

					foreach ( $this->incoming  as $key => $value) {

						if ($value['visible'] == 1) {

							if (strtolower($value['type']) == 'single') {
								
								if (strtolower($value['domains']) == 'all') {
									
									$menu[] = $value;

								}else{
									if (strtolower($value['domains']) == strtolower($session['user_userGroup'])) {
										if (  $value['security']['level'] >=  $session['level']) {

											$menu[] = $value;

										}
									}else{

										if(isset($value['security']) && !empty($value['security'])){
											// $this->securityChecker($value);
											if (!empty($value['security']['level'])) {

												if (str_contains(strtolower($value['security']['views']), strtolower($session['user_userGroup'])) || str_contains(strtolower($value['security']['views']), strtolower('all') ) ) {

													if (  $value['security']['level'] >=  $session['level']) {

														$menu[] = $value;

													}
												}
											}else{

												if (str_contains(strtolower($value['security']['views']), strtolower($session['user_userGroup'])) || str_contains(strtolower($value['security']['views']), strtolower('all'))){
													$menu[] = $value;
												}elseif(strtolower($value['security']['views']) == 'all'){
													$menu[] = $value;
												}



											}

										}
									}

								}#not all domain


							}else{

								$menu[] = $this->processSubMenu($value);
								 // $value;
							}

						}else{
							$disabled[] = $value;
						}
					}
				else:

					foreach ($this->default as $key => $value) {

						// $menu[] = $value;
						// var_dump($value);


					}

				endif;

			else:

				// logout or pull down the menu for the user
				// this means the user is nto logged in

			endif;

			
			// var_dump($session);
			// print('<hr>');
			// var_dump($this->user_data);
			// // print('<hr>');
			// var_dump($this->default);
			// print('<hr>');
			$this->done_menu = $menu;
		}

		public function getMenu($data){
			$this->user_data = $data;
			if (\Auth::check()) {
				$this->processMenu();
			}			
			return $this->done_menu;
		}

		private function processSubMenu($value){
			$menu = array();
			$roles = \UserRole::where('user_id','=',$this->user_data->id)->first();
			if (!empty($roles)) {
				$menu['name'] = ucwords($roles->fullname);
				$menu['visible'] = 1;
				$menu['url'] = $this->user_data->user_url ?: '#';
				$menu['type'] = 'drop';
				foreach ($value['menu'] as $d => $drop) {
					if ($drop['visible'] == 1) {
						// $menu['menu'][] = $drop;
						if ($drop['name'] == 'settings') {
							$drop['url'] = 'users/'.$this->user_data->id.'/edit';
						}
						// if ($drop['name'] == 'logout') {
						// 	$drop['url'] = 'users/'.$this->user_data->id.'/edit';
						// }
						$menu['menu'][] = $drop;
					}
				}
			}
			// var_dump($menu);
			return $menu;
		}
		// private function securityChecker($value){
		// 	$menu = array();
		// 	if (!empty($value)) {
		// 		$session
		// 	}
		// }
}
