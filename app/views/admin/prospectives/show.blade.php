<?php #page specific processing
    $address = [];
    $person = [];
    $contacts = [];
    $plot = [];
    if(isset($data) && !empty($data)):
    endif;
 ?>
@include('templates/top-admin')
@section('content')
  <div class="cc" id="transactions">
    <h3>Prospective Records</h3>
    <hr>
    <table class="table table-bordered">
      <thead>
        <tr>
            <th>course name</th>
            <th>course lecturer</th>
            <th>duration</th>
            <th>admission date</th>
            <th>award</th>
            <th>update</th>
            <th>create</th>
        </tr>
      </thead>
      <tbody>
        <?php if (!empty($data['courses'])): ?>
            <?php foreach ($data['courses'] as $key => $value): ?>
            <tr>
                <td>{{ucwords($value['name'])}}</td>
                <td>{{ucwords($value['staff']['Staff_MainLevelTeaching'])}}</td>
                <td>{{ucwords($value['duration'])}}</td>
                <td>{{ucwords($data['admission_date'])}}</td>
                <td>{{ucwords($value['qualification'])}}</td>
                <td>{{ucwords($value['updated_at'])}}</td>
                <td>{{ucwords($value['created_at'])}}</td>
            </tr>                                               
            <?php endforeach ?>
        <?php endif ?>
      </tbody>
    </table>
  </div>
@stop
@include('templates/bottom-admin')