<?php namespace Universal\DailyTransaction;

/**
* 
*/
use Universal\DailyTransaction\ProcessPayment;
class PayRent extends ProcessPayment
{
	protected $data;
	protected $userInput;
	protected $mons = ['1'=>'jan', '2'=>'feb', '3'=>'mar', '4'=>'apr', '5'=>'may', '6'=>'jun', '7'=>'jul', '8'=>'aug', '9'=>'sep', '10'=>'oct', '11'=>'nov', '12'=>'dec'];
	function __construct($trans)
	{
		$this->data = $trans;
		$this->userInput = \Input::all();
	}
}
