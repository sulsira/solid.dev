<?php #page specific processing

	$agents = (isset($trans['agents']))? $trans['agents'] : [];
	$keys = $trans['keys'] ?: [];
	$payments =  [];
	if(isset($trans['raw']) && !empty($trans['raw'])):
		foreach ($trans['raw'] as $keys => $value) {
			foreach ($value as $k => $pay) {
				$payments[] = $pay;
			}
		}
	endif;
	if(isset($trans['results']) && !empty($trans['results'])):
		foreach ($trans['results'] as $keys => $value) {
			foreach ($value as $k => $pay) {
				$payments[] = $pay;
			}
		}
	endif;
	// dd($trans);
 ?>
@include('templates/top-admin')
@section('content')
@include('__partials/modal-add-trans')
	<div class="c-header cc">
		<h3>Daily transactions <a href="#myModal" role="button" data-toggle="modal">	<i class="fa fa-plus"></i></a></h3>

	</div>
	<div class="cc">
	<div class="datesearch">
		{{Form::open(['route'=>'checkdate','method'=>'get'])}}
		{{Form::number('day',null,['placeholder'=>'day','value'=>'day','class'=>"span1", 'step'=>'1','max'=>'31','min'=>'0'])}}
		<select name="month" id="month" class="span2">
			<option value="0" selected="1">Month</option>
			<option value="1">Jan</option>
			<option value="2">Feb</option>
			<option value="3">Mar</option>
			<option value="4">Apr</option>
			<option value="5">May</option>
			<option value="6">Jun</option>
			<option value="7">Jul</option>
			<option value="8">Aug</option>
			<option value="9">Sep</option>
			<option value="10">Oct</option>
			<option value="11">Nov</option>
			<option value="12">Dec</option>
		</select>
		{{Form::number('year',null,['placeholder'=>'year','value'=>'year','class'=>"span2",'required'=>1,'min'=>'1000'])}}
		{{Form::hidden('type','adate')}}
		{{Form::submit('check',['class'=>"checkdate btn"])}}
		{{Form::close()}}
	</div>
	<hr>
	<div class="dateselect">
		{{Form::open(['route'=>'checkdate','method'=>'get'])}}
		{{Form::label('day','From')}}
		{{Form::number('from[day]',null,['placeholder'=>'day','value'=>'day','class'=>"span1", 'step'=>'1','max'=>'31','min'=>'0'])}}
		<select name="from[month]" id="month" class="span2">
			<option value="0" selected="1">Month</option>
			<option value="1">Jan</option>
			<option value="2">Feb</option>
			<option value="3">Mar</option>
			<option value="4">Apr</option>
			<option value="5">May</option>
			<option value="6">Jun</option>
			<option value="7">Jul</option>
			<option value="8">Aug</option>
			<option value="9">Sep</option>
			<option value="10">Oct</option>
			<option value="11">Nov</option>
			<option value="12">Dec</option>
		</select>
		{{Form::number('from[year]',null,['placeholder'=>'year','value'=>'year','class'=>"span2",'required'=>1,'min'=>'1000'])}}
		{{Form::hidden('type','dates')}}
		<span>To</span>
		{{Form::number('to[day]',null,['placeholder'=>'day','value'=>'day','class'=>"span1", 'step'=>'1','max'=>'31','min'=>'0'])}}
		<select name="to[month]" id="month" class="span2">
			<option value="0" selected="1">Month</option>
			<option value="1">Jan</option>
			<option value="2">Feb</option>
			<option value="3">Mar</option>
			<option value="4">Apr</option>
			<option value="5">May</option>
			<option value="6">Jun</option>
			<option value="7">Jul</option>
			<option value="8">Aug</option>
			<option value="9">Sep</option>
			<option value="10">Oct</option>
			<option value="11">Nov</option>
			<option value="12">Dec</option>
		</select>
		{{Form::number('to[year]',null,['placeholder'=>'year','value'=>'year','class'=>"span2",'required'=>1,'min'=>'1000'])}}
		{{Form::submit('check',['class'=>"checkdate btn"])}}
		{{Form::close()}}
	</div>
	</div>
<div class="cc">
<?php if (!empty($trans['keys']) && count($trans['keys']) > 1): ?>
 <?php 
	$date1 = new DateTime(min($trans['keys']));
	$date2 = new DateTime(max($trans['keys']));
  ?>
  <h4>Dates: </h4><span><?php 
echo $date1->format('d-M-Y') .' To '.$date2->format('d-M-Y');
   ?></span>	
<?php endif ?>

	<hr>
	@include('__partials/errors')
	    	<table class="table table-hover table-bordered">
	        <thead>
	            <tr>
	                <th colspan="8">Details</th>
	                <th>Payment</th>
	                <!-- <th>Actions</th> -->
	            </tr>
	            <tr>
	            	<th>Date</th>
	            	<th>Description</th>
	            	<th>Type</th>
	            	<th>For:</th>
	            	<th>Locate</th>
	            	<th>Agent</th>
	            	<th>Customer</th>
	            	<th>Ref#</th>
	            	<th>Amount</th>
	            	<th>Options</th>
	            </tr>
	        </thead>
	        <tbody>
	        <?php $total = 0; $expenses = 0;?>
					<?php if (isset($trans['results'])): ?>
							<?php if (!empty($payments)): ?>
								<?php foreach ($payments as $pp => $payment): ?>
								<tr>
									<td>
										{{
											ucwords($payment['date_paid'])
										}}
									</td>
									<td>
										{{
											ucwords($payment['description'])
										}}
									</td>

									<td>
										{{
											ucwords($payment['type'])
										}}
									</td>
									<td>
										{{
											ucwords($payment['paid_for'])
										}}
									</td>

									<td>
										{{
											ucwords($payment['locationOrProperty'])
										}}
									</td>
									<td>
										<a href="{{route('agents.show',$payment['agentID'])}}">
											{{
												ucwords($payment['agent']['person']['pers_fname'].' '.$payment['agent']['person']['pers_mname'].' '.$payment['agent']['person']['pers_lname'])
											}}
										</a>
									</td>
									<td>
										{{
											ucwords($payment['customer'])
										}}
									</td>

									<td>
										{{
											ucwords($payment['ref'])
										}}
									</td>

									<td>
										{{
											ucwords($payment['amount'])
										}}
										<?php 
										if (strtolower($payment['type']) == 'income') {
											$total += $payment['amount'];
										}elseif (strtolower($payment['type']) == 'expense'){
											$expenses += $payment['amount'];
										}
										
										 ?>
									</td>


								<td class="actions">
									
								<a href="#" class="dropdown-toggle"  data-toggle="dropdown">
									Actions
								</a>
								  <ul class="dropdown-menu">
								    <!-- dropdown menu links -->
								    <li><a href="#myModal_<?php echo $payment['id']; ?>"  role="button" data-toggle="modal">edit</a> </li>
								    <li>
								    	{{Form::delete('paying/'. $payment['id'], 'Delete')}}
								    </li>
								  </ul>
								</td>
								</tr>						
								<?php endforeach ?>
								<?php else: ?>
								<tr>
									<td colspan="9"><h4>No payments Available!</h4></td>
								</tr>
							<?php endif ?>	
						<?php else: ?>
							<?php if (!empty($payments)): ?>
								<?php foreach ($payments as $pp => $payment): ?>
								<div id="myModal_<?php echo $payment['id']; ?>" class="modal hide fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
								  <div class="modal-header">
								    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
								    <h3 id="myModalLabel">Edit payment</h3>
								  </div>
								{{Form::model($payment, ['route'=>['paying.update',$payment['id']],'method'=>'PATCH'],['class'=>'form-snippet'])}}
		  <div class="modal-body">
					<div class="level details">
						<div class="first">
							<div>
								{{Form::label('type','Type')}}
								<select name="type" id="enit" class="input-xlarge">
								<option value="{{$payment['type']}}">{{$payment['type']}}</option>
									<option>income</option>
									<option>expense</option>
								</select>
							</div>
							<div>
								{{Form::label('paid_for','For')}}
								<select name="paid_for" id="enit" class="input-xlarge">
									<option value="{{$payment['paid_for']}}">{{$payment['paid_for']}}</option>
									<optgroup label="Incomes">
										<option value="rent">Rent</option>
										<option value="mortgage">Mortgage</option>
										<option value="salescommision">Sales Commision</option>
									</optgroup>
									<optgroup label="Expenses">
										<option value="exp_cashpower">Cash Power</option>
										<option value="exp_carservice">Car Service</option>
										<option value="exp_advertising">advertising</option>
										<option value="exp_salary">Salary</option>
										<option value="exp_tax">Tax</option>
										<option value="exp_commission">Commission</option>
										<option value="exp_drawings">Drawings</option>
										<option value="exp_fuel">Fuel</option>
										<option value="exp_far">Fare</option>
										<option value="exp_rent">rent</option>
									</optgroup>
								</select>
							</div>
							<div>
								{{Form::label('customer','Customer')}}
								{{Form::text('customer',null,['class'=>'input-xlarge','placeholder'=>'Enter customer name', 'required'=>1])}}
							</div>
							<div>
								{{Form::label('house_plot_num','House or plot #')}}
								{{Form::text('house_plot_num',null,['class'=>'input-xlarge','placeholder'=>'Enter house or plot number', 'required'=>1])}}
							</div>

							<div>
								{{Form::label('date_paid','Date')}} *{{$payment['date_paid']}}*
								{{Form::date('date_paid',['required'=>1])}}
							</div>
							<div>
								{{Form::label('locationOrProperty','Location/property')}}
								{{Form::text('locationOrProperty',null,['class'=>'input-xlarge','placeholder'=>'Enter Location or property'])}}
							</div>
							<div>
								{{Form::label('description','Description')}}
								{{Form::text('description',null,['class'=>'input-xlarge','placeholder'=>'Description'])}}
							</div>
							<div>
								{{Form::label('ref','Ref #')}}
								{{Form::text('ref',null,['class'=>'input-xlarge','placeholder'=>'reference number'])}}
							</div>

						</div>
					</div>

					<div class="level details">
						<span>Monetary </span>
						<hr>
						<div class="first ">
							<div>
								{{Form::label('amount','Amount')}}
								{{Form::number('amount',null,['class'=>'input-xlarge span8','placeholder'=>'Enter Amount being paid','step'=>'any', 'required'=>1])}}
							</div>
						</div>
					</div>
		  </div>
											<div class="modal-footer">
													<button class="btn" data-dismiss="modal" aria-hidden="true">Close</button>
													<button class="btn btn-primary" name="createUser">Save changes</button>
											</div><!-- end of modal footer -->		
									 		{{Form::close()}}
										  </div>
								<tr>
									<td>
										{{
											ucwords($payment['date_paid'])
										}}
									</td>
									<td>
										{{
											ucwords($payment['description'])
										}}
									</td>

									<td>
										{{
											ucwords($payment['type'])
										}}
									</td>
									<td>
										{{
											ucwords($payment['paid_for'])
										}}
									</td>

									<td>
										{{
											ucwords($payment['locationOrProperty'])
										}}
									</td>
									<td>
										<a href="{{route('agents.show',$payment['agentID'])}}">
											{{
												ucwords($payment['agent']['person']['pers_fname'].' '.$payment['agent']['person']['pers_mname'].' '.$payment['agent']['person']['pers_lname'])
											}}
										</a>
									</td>
									<td>
										{{
											ucwords($payment['customer'])
										}}
									</td>

									<td>
										{{
											ucwords($payment['ref'])
										}}
									</td>

									<td>
										{{
											ucwords($payment['amount'])
										}}
										<?php 
										if (strtolower($payment['type']) == 'income') {
											$total += $payment['amount'];
										}elseif (strtolower($payment['type']) == 'expense'){
											$expenses += $payment['amount'];
										}
										
										 ?>
									</td>
								<td class="actions">
									
								<a href="#" class="dropdown-toggle"  data-toggle="dropdown">
									Actions
								</a>
								  <ul class="dropdown-menu">
								    <!-- dropdown menu links -->
								    <li><a href="#myModal_<?php echo $payment['id']; ?>"  role="button" data-toggle="modal">edit</a> </li>
								    <li>
								    	{{Form::delete('paying/'. $payment['id'], 'Delete')}}
								    </li>
								  </ul>
								</td>
								</tr>						
								<?php endforeach ?>
								<?php else: ?>
								<tr>
									<td colspan="9"><h4>No payments Available!</h4></td>
								</tr>
							<?php endif ?>							
					<?php endif ?>
	        </tbody>
	        <tfoot>
	        	<tr>
	        		<th>Total Income</th>
	        		<th></th>
	        		<th></th>
	        		<th></th>
	        		<th></th>
	        		<th></th>
	        		<th></th>
	        		<th colspan="2"><var>
	        			<?php 
	        			echo 'D '.$total;
	        			 ?>
	        		</var></th>
	        	</tr>
	        	<tr>
	        		<th>Total Expenses</th>
	        		<th></th>
	        		<th></th>
	        		<th></th>
	        		<th></th>
	        		<th></th>
	        		<th></th>
	        		<th colspan="2"><var>
	        			<?php 
	        			echo 'D '.$expenses;
	        			 ?>
	        		</var></th>
	        	</tr>
	        	<tr>
	        		<th>Total (Income - Expenses)</th>
	        		<th></th>
	        		<th></th>
	        		<th></th>
	        		<th></th>
	        		<th></th>
	        		<th></th>
	        		<th colspan="2"><var>
	        		D 
	        			<?php 
	        			echo ($total - $expenses);
	        			 ?>
	        		</var></th>
	        	</tr>
	        </tfoot>
	   		</table>
</div>

		</div>
	</div>
@stop
@include('templates/bottom-admin')