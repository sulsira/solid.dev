<?php #page specific processing ?>
@include('templates/top-admin')
@section('content')
	<div class="c-header cc">
		<h3>Editing : <?php echo (!empty($estate))? ucwords($estate['name']) :'Unknow Estate'; ?></h3>
	</div>
	<div class="cc">
		<hr>
		  {{Form::model($estate,[ 'route'=>['estates.update', $estate->est_id] , 'method'=>'PATCH' ],['class'=>'form-snippet'])}}
		  <div class="modal-body">
				
					<div class="level name">

						<div>
							{{Form::label('name','Estate name')}}
							{{Form::text('name',null,['class'=>'input-xlarge span6','placeholder'=>'Enter estate name'])}}
						</div>
						<div>
							{{Form::label('location','Location')}}
							{{Form::text('location',null,['class'=>'input-xlarge span6','placeholder'=>'Enter location'])}}
						</div>
					</div>
		  </div>
			<div class="modal-footer">
					<button class="btn" data-dismiss="modal" aria-hidden="true">Close</button>
					<button class="btn btn-primary" name="createUser">Save changes</button>
			</div><!-- end of modal footer -->		
	 {{Form::close()}}
	</div>
@stop
@include('templates/bottom-admin')