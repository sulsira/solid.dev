<?php

class House extends \Eloquent {
	protected $primaryKey = 'hous_id';
	protected $fillable = [
		'hous_numberOfrooms',
		'hous_number',
		'hous_description',
		'hous_id',
		'hous_compoundID',
		'hous_tenantID',
		'hous_advance',
		'hous_price',
		'hous_paymentStype',
		'hous_availability', #available or not-available
		'hous_status', # 0 - not occupied, 1 = mean occupied, 2 = pending to be empty, 3 = not for renting;
		'deleted'
	];

	public function compound(){
		return $this->belongsTo('Compound','hous_compoundID','comp_id');
	}
	public function scopeAvailable($query){
		return  $query->whereRaw('hous_tenantID = ? OR hous_availability = ? OR hous_status = ? Or  hous_availability = ?',[0,'available', 0, 'null'])->get();
	}
	public function tenants(){
		return $this->hasMany('Tenant','tent_houseID','hous_id');
	}
}