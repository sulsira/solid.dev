<?php namespace Universal\Forms;

use Laracasts\Validation\FormValidator;

class CreateDailyTransaction extends FormValidator{


		/**
		 * validation rules for the plot creation form
		 * Post /plots/create
		 *
		 * @return Response 
		 */
	protected $rules = [
			  'type' => 'max:200',
			  'paid_for' => 'max:200',
			  'customer' => 'max:200',
			  'house_plot_num' => 'required',
			  'date_paid' => 'required',
			  'locationOrProperty' => 'max:200',
			  'description' => 'max:250',
			  'ref' => 'max:200',
			  'amount' => 'required|numeric|max:99999999999999999'
	];


}