<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class AddAmountPaidLuwasTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('luwas', function(Blueprint $table)
		{
			$table->decimal('amount_paid', 20,3)->default(0);
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('luwas', function(Blueprint $table)
		{
			$table->dropColumn('amount_paid');
		});
	}

}
