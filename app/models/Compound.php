<?php

class Compound extends \Eloquent {
	protected $primaryKey = 'comp_id';
	protected $fillable = [
	'comp_id',
	'comp_landLordID',
	'comp_numberOfHouses',
	'comp_size',
	'comp_remarks',
	'location',
	'comp_indentifier',
	'name',
	'agendid'
	];


	public function scopeAvailable($query){
		return $query->whereRaw('deleted != ? AND comp_landLordID = ?',[0,0])->get();
	}

	public function landlord(){
		return $this->belongsto('Landlord','comp_landLordID','id');
	}
	public function houses(){
		return $this->hasMany('House','hous_compoundID','comp_id');
	}
	public function agent(){
		return $this->belongsTo('Agent','agendid','agen_id');
	}
}