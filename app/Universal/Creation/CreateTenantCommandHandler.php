<?php namespace Universal\Creation;
/**
* 
*/
use Laracasts\Commander\CommandHandler;
use Universal\Tenants\TenantRepository;
use Laracasts\Commander\Events\DispatchableTrait;
class CreateTenantCommandHandler implements CommandHandler
{
	use DispatchableTrait;
	protected $repository;
	function __construct(TenantRepository $repository){
		$this->repository = $repository;
	}
	public function handle($command){
		$tenant  = \Tenant::creation($command);
		$this->repository->save($tenant);
		// $events = $tenant->releaseEvents();
		$this->dispatchEventsFor($tenant);

	}
}
