<?php
use Laracasts\Commander\Events\EventGenerator;
use Universal\Creation\Events\TenantCreated;
class Tenant extends \Eloquent {
use EventGenerator;
	protected $primaryKey = 'tent_id';
	protected $fillable = [
				'tent_id',
				'tent_houseID',
				'tent_compoundID',
				'tent_rentID',
				'tent_personid',
				'tent_advance',
				'tent_monthlyFee',
				'tent_paymentStype',
				'tent_status'  # 0 - left, 1 = mean occupying, 2 = pending to leave, 3 = owed rent before leaving; 4 = does not have a house
			];

	public function house(){
		return $this->belongsTo('House','tent_id','hous_tenantID');
	}
	public function person(){
		return $this->belongsTo('Person','tent_personid','id');
	}
	public function rents(){
		return $this->hasMany('Rent','rent_tenantID','tent_id');
	}
	public function documents(){
		return $this->hasMany('Document','entity_ID','tent_id');
	}
	public function luwas(){
		return $this->hasMany('Luwa','tenant_id','tent_id');
	}
	public static function creation($all){
		$input = [];
 		foreach($all as $key => $value)	$input = $value;
		$person = array();
		$staff = array();
		$address = array();
		$contact = array();
		$tent = array();
		$done = false;

		if ($input) :
			foreach ($input as $k => $table) {
				if ($k) {

					if ($k == 'person') {
						
							$person = Person::create(array(
							'pers_fname' => $table['fname'], 
							'pers_mname' => ($table['mname']) ?: null, 
							'pers_lname' => $table['lname'], 
							'pers_type' => 'Test Tenant', 
							'pers_DOB' => $table['dob'], 
							'pers_gender' => $table['Pers_Gender'], 
							'pers_nationality' => $table['Pers_Nationality'], 
							'pers_ethnicity' => $table['Pers_Ethnicity'], 
							'pers_NIN' => $table['nin_id'] 
							));
					}

					if ($k == 'address') {
						$address = $table;
						if ($person->id) {
							$address = array_add($address, 'Addr_EntityID', $person->id);
							$address = array_add($address, 'Addr_EntityType', 'Person');
							$address = Address::create($address);
						}
					}

					if ($k == 'contact') {

						

						if ($person->id) {

							foreach ($table as $key => $value) {

									if(!empty($value)){
										$contact = Contact::create(array(
										'Cont_EntityID' => $person->id,	
										'Cont_EntityType' => 'Person',	
										'Cont_Contact' => $value,	
										'Cont_ContactType' =>  $key	
										));

									$contact = $contact->toArray();
									}
							}
						}
					}

				}#end of k
				$done = true;
			}#end of foreach

			if ($done):	

					if ( !empty($input['houseID']) || !empty($input['Monthly_fee']) ) {


							$house = House::where('hous_id','=',$input['houseID'])->first();
							$house = (!empty($house))? $house->toArray() : [];
			
											
							if (!empty($house)) {
								$tent = Tenant::create(array(
										'tent_houseID'=> $input['houseID'],
										// 'tent_compoundID'=> $house['hous_compoundID'],
										'tent_advance'=> $input['down_payment'],
										'tent_personid'=> $person->id,
										'tent_status'=> 1
								));
							}else{

								$tent = Tenant::create(array(
										// 'tent_houseID'=> $input['houseID'],
										// 'tent_compoundID'=> $house['hous_compoundID'],
										'tent_advance'=> $input['down_payment'],
										'tent_personid'=> $person->id,
										'tent_status'=> 4 #does not have a house
								));	
											
							}

							// we add rent 
							$montlyfee = 0;   
							// dd($house);
							if( isset($input['Monthly_fee']) && !empty($input['Monthly_fee']) ):
								$montlyfee = $input['Monthly_fee'];
							else:
								if(!empty($house)):
				  					$montlyfee = $house['hous_price'] ;
				  				endif;
							endif;
							$ren = Rent::create(array(
								'rent_houseID' => $input['houseID'],
								'rent_tenantID' => $tent->tent_id,
								'rent_advance' => $input['down_payment'],
								'rent_firstmonthpaid' => null,
								'rent_nextpaydate' => null,
								'rent_monthlyFee' => $montlyfee
							));
							
							$house = House::find($input['houseID']);
							$tenanting = Tenant::find($tent->tent_id);
							if(!empty( $tenanting )):

								$tenanting->tent_rentID = $ren->rent_id;
								$tenanting->save();


								if(!empty( $house )):
									$house->hous_status = 1;
									$house->hous_availability = 'not-available';
									$house->hous_tenantID = (!empty($tent->tent_id))? $tent->tent_id : 0;
									$house->save();
								endif;

							endif;

			
					} #end of monthly fee and house
					else{
						# here the tenant have no house and no payment was made
						$tent = Tenant::create(array(
						'tent_personid'=> $person->id,
						'tent_status'=> 4
						));
					}
			endif; #if done is true

		endif;

		// raising a notification
		$tent->raise(new TenantCreated($tent));
		return $tent;
	}
}