<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateRentPaysTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('RentPays', function(Blueprint $table)
		{
			$table->engine = 'MyISAM';
			$table->increments('id');
			$table->integer('tenant_id')->default(0);
			$table->integer('ll_id')->default(0);
			$table->integer('compound_id')->default(0);
			$table->integer('year')->default(0);
			$table->decimal('jan',20,8)->default(0);
			$table->decimal('feb',20,8)->default(0);
			$table->decimal('mar',20,8)->default(0);
			$table->decimal('apr',20,8)->default(0);
			$table->decimal('may',20,8)->default(0);
			$table->decimal('jun',20,8)->default(0);
			$table->decimal('jul',20,8)->default(0);
			$table->decimal('aug',20,8)->default(0);
			$table->decimal('sep',20,8)->default(0);
			$table->decimal('oct',20,8)->default(0);
			$table->decimal('nov',20,8)->default(0);
			$table->decimal('dec',20,8)->default(0);
			$table->integer('status')->default(0);
			$table->boolean('deleted');
			$table->timestamps();
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('RentPays');
	}

}
