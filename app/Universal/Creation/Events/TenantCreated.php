<?php namespace Universal\Creation\Events;

 /**
* 
*/
use Tenant;
class TenantCreated
{
	public $tenant;

	function __construct(Tenant $tenant)
	{
		$this->tenant = $tenant;
	}
} 