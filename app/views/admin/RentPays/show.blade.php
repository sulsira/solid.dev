<?php #page specific processing
    $image = array();
    $person =  array();
    $documents =  array();
    $compound = array();
    if (!empty($tenant)) {
        foreach ($tenant as $key => $value) {
           if ($key == 'person') {
              $person = $value;
           }
           if ($key == 'documents') {
               foreach ($value as $d => $doc) {
                    if ($doc['type'] == 'Photo') {
                       $image = $doc;
                    }else{
                        $documents[] = $doc;
                    }
               }
           }
           if ($key == 'compound') {
               $compounds = $value;
           }
           if ($key == 'kin') {
               $kin = $value;
           }
        }
    }
    $contacts = (!empty($person['contacts']))? $person['contacts'] : [];
    $addresses = (!empty($person['addresses']))? $person['addresses'] : [];
    if (!empty($person['documents'])) {
       foreach ($person['documents'] as $d => $doc) {
            if ($doc['type'] == 'Photo') {
               $image = $doc;
            }else{
                $documents[] = $doc;
            }
       }
    }
  $rent =  !empty(last($tenant['rents']))? last($tenant['rents']) : [];
  $newhouse  = House::available();
//   var_dump($tenant);
// die(var_dump($newhouse->toArray()));
  $mons = ['1'=>'jan', '2'=>'feb', '3'=>'mar', '4'=>'apr', '5'=>'may', '6'=>'jun', '7'=>'jul', '8'=>'aug', '9'=>'sep', '10'=>'oct', '11'=>'nov', '12'=>'dec'];
  // for($i = 0; $i <= 12; $i++){

  // }
 ?>
@include('templates/top-admin')
@section('content')
@include('__partials/add-to-house')
@include('__partials/modal-add-payment')
@include('__partials/errors')
   <div class="scope">
        <div class="hedacont">
            <div class="navbar">
                <div class="navbar-inner" id="scopebar">
                    <div class="container">
                        <a class="btn btn-navbar" data-toggle="collapse" data-target="navbar-responsive-collapse">
                          <span class="icon-bar"></span>
                          <span class="icon-bar"></span>
                          <span class="icon-bar"></span>
                        </a>
                        <a class="brand" href="{{route('tenants.show',$tenant['tent_id'])}}">Tenant Name : {{ucwords($tenant['person']['pers_fname'].' '.$tenant['person']['pers_mname'].' '.$tenant['person']['pers_lname'])}}</a>
                        <div class="nav-collapse collapse navbar-responsive-collapse">
                          <ul class="nav">  
                            <li><a href="{{route('tenants.show',$tenant['tent_id'])}}#basic">General</a> </li>
                            <li><a href="{{route('tenants.show',$tenant['tent_id'])}}#rent">Rent</a></li>
                            <li><a href="{{route('tenants.show',$tenant['tent_id'])}}#payments">Payments</a></li>
                            <li><a href="{{route('tenants.show',$tenant['tent_id'])}}#house">House</a></li>
                            <li>{{HTML::link('/tenants/'.$tenant['tent_id'].'/rents-paid','Rent-payment')}}</li> 
                            <!-- <li><a href="#documents">Documents</a></li> -->
                            <li><a href="{{route('tenants.edit',$tenant['tent_id'])}}">Edit</a> </li>
                            <li class="divider-vertical"></li>
                              
                            <li class="dropdown">
                            <a href="#" class="dropdown-toggle" data-toggle="dropdown">Options <b class="caret"></b></a>
                            <ul class="dropdown-menu">
                                @if(empty($tenant['tent_houseID']))
                                  <li>
                                   <a href="#add-to-house" role="button" data-toggle="modal">Add to a house</a>
                                  </li>
                                  @else
                                  <!-- <li><a href="#">Transfer to another house</a></li> -->
                                @endif
                              <li><a href="{{route('vacate',[$tenant['tent_id'],$tenant['house']['hous_id']])}}">Vacate</a></li>
                              <li class="divider"></li>
                              <li><a href="{{route('kill',$tenant['tent_id'])}}">Delete Records</a></li>
                            </ul>
                          </li>

                           </ul>
                        </div><!-- /.nav-collapse -->
                    </div>
                </div><!-- /navbar-inner -->
            </div> 
            <div class="c-header">
                <?php if (!empty($image)): ?>
                    <ul class="thumbnails" id="thmb">
                        <li class="span2">
                          <a href="#" class="thumbnail">
                           {{HTML::image($image['thumnaildir'])}}
                          </a>
                        </li>
                    </ul>                      
                <?php endif ?>

            </div>          
        </div>  
    </div>  <!-- end of scope -->

    <div class="content-details clearfix">



   <div class="cc clearfix" id="payments">
        <h3>Monthly rents </h3>
    <hr>
    <table class="table table-bordered">
      <thead>
        <tr>
            <th>Compounds</th>
            <th>Year</th>
            <th colspan="12">Months</th>
            <th colspan="3">Payments</th>
        </tr>
        <tr>
            <th></th>
            <th></th>
            <th>jan</th>
            <th>feb</th>
            <th>mar</th>
            <th>apr</th>
            <th>may</th>
            <th>jun</th>
            <th>jul</th>
            <th>aug</th>
            <th>sep</th>
            <th>oct</th>
            <th>nov</th>
            <th>dec</th>
            <th>Overpay</th>
            <th>Arrears</th>
            <th>Total</th>
        </tr>
      </thead>
      <tbody>
        <?php if (isset($tenant['rent-payment']) && !empty($tenant['rent-payment'])): ?>
          <?php foreach ($tenant['rent-payment'] as $key => $value): ?>
            <tr>

              <?php 
              $total = 0;
              $arrears = 0;
              $paid = 0;
              $balance = 0;
               ?>

              <td>
                <?php if (isset($tenant['house']['compound']) && !empty($tenant['house']['compound'])): ?>
                  <a href="{{route('compounds.show',$tenant['house']['compound']['comp_id'])}}">{{$tenant['house']['compound']['comp_indentifier']}}</a>
                <?php else: ?>
                  -
                <?php endif ?>

              </td>
              <td>
                {{$key}}
              </td>
              <?php foreach ($mons as $index => $mth): ?>
                <td>
                <?php if (isset( $value[$mth]['month_short'] )): ?>
                   <?php if ($mth == $value[$mth]['month_short']): ?>
                     {{$value[$mth]['amount_paid']}}
                     <?php 
                        // calculating areas and others
                      if ( $value[$mth]['amount_paid'] !== $value[$mth]['amount'] ) {

                        if( $value[$mth]['amount_paid'] < $value[$mth]['amount'] ){

                          $arrears = $arrears + ($value[$mth]['amount_paid'] - $value[$mth]['amount']);

                        }
                        if( $value[$mth]['amount_paid'] > $value[$mth]['amount'] ){

                          $balance =  $balance + ($value[$mth]['amount_paid'] - $value[$mth]['amount']);

                        }


                      }

                      $total = $total + $value[$mth]['amount_paid'];
                      ?>
                    <?php else: ?>
                     <?php 
                        // calculating areas and others
                      if ( $value[$mth]['amount_paid'] !== $value[$mth]['amount'] ) {

                        if( $value[$mth]['amount_paid'] < $value[$mth]['amount'] ){

                          $arrears = $arrears + ($value[$mth]['amount_paid'] - $value[$mth]['amount']);

                        }
                        if( $value[$mth]['amount_paid'] > $value[$mth]['amount'] ){

                          $balance =  $balance + ($value[$mth]['amount_paid'] - $value[$mth]['amount']);

                        }


                      }

                      $total = $total + $value[$mth]['amount_paid'];
                      ?>                      
                   <?php endif ?>                  
                <?php endif ?>

                </td>
              <?php endforeach ?>
              <td>{{$balance}}</td>
              <td>{{$arrears}}</td>
              <td>{{$total}}</td>
            <?php endforeach ?>              

            </tr>

        <?php else: ?>
         <tr><td colspan="17"><h4>No Payment yet!</h4></td></tr>
        <?php endif ?>
      </tbody>
    </table>
   </div>

</div>


@stop
@include('templates/bottom-admin')