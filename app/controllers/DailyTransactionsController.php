<?php
use Universal\Core\CommandBus;
use Universal\Forms\CreateDailyTransaction; 
use Universal\Creation\DailyTransactionCommand; 
class DailyTransactionsController extends BaseController {

	private $dailytransaction;
	/**
	 * Display a listing of the resource.
	 * GET /dailytransactions
	 *
	 * @return Response
	 */
	use CommandBus;
	protected $layout = 'admin';
	function __construct(CreateDailyTransaction $dailytransaction){
		$this->dailytransaction = $dailytransaction;
	}
	public function index()
	{
		//
	}

	/**
	 * Show the form for creating a new resource.
	 * GET /dailytransactions/create
	 *
	 * @return Response
	 */
	public function create()
	{
		//
	}

	/**
	 * Store a newly created resource in storage.
	 * POST /dailytransactions
	 *
	 * @return Response
	 */
	public function store()
	{
		// validate using global scope
			$this->dailytransaction->validate(Input::all());
		// send validated input to TDO
			$command = new DailyTransactionCommand() ; //TDO

		// Use commander to  execute the inputs fields
			$this->execute($command);

		// use a repository to save up to database


		// raise and event announcement

		// go back

		Flash::message("Successfully made a payment");
		return Redirect::back();
		// die();
		// die();
		// $input = Input::all();

		// $v = Validator::make($input,['amount'=>'required']);
		// if ($v->fails()):
		// 	return Redirect::back()->withInput();	
		// endif;
		// $df = new DailyTransaction;
		// $df->fill($input);
		// $df->save();

		// 		Flash::message("Successfully made a payment");
		// $df->raise(new DailyPaid($df));
		// 		return Redirect::back();
	}

	/**
	 * Display the specified resource.
	 * GET /dailytransactions/{id}
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show($id)
	{
		//
	}

	/**
	 * Show the form for editing the specified resource.
	 * GET /dailytransactions/{id}/edit
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit($id)
	{
		//
	}

	/**
	 * Update the specified resource in storage.
	 * PUT /dailytransactions/{id}
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update($id)
	{
		$this->dailytransaction->validate(Input::all());
		$dt = DailyTransaction::findOrFail($id);
		$dt->fill(Input::all());
		$dt->save();

		Flash::message("Successfully updated a payment");
		// $df->raise(new DailyPaid($df));
		return Redirect::back();
	}

	/**
	 * Remove the specified resource from storage.
	 * DELETE /dailytransactions/{id}
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($id)
	{
		DailyTransaction::destroy($id);
		return Redirect::back();
	}

}