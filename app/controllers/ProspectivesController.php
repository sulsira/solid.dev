<?php

class ProspectivesController extends \AdminController {

	/**
	 * Display a listing of the resource.
	 * GET /prospectives
	 *
	 * @return Response
	 */
	public function index()
	{
		$this->layout->content = View::make('admin.prospectives.index');
	}

	/**
	 * Show the form for creating a new resource.
	 * GET /prospectives/create
	 *
	 * @return Response
	 */
	public function create()
	{
		$this->layout->content = View::make('admin.prospectives.create');
	}

	/**
	 * Store a newly created resource in storage.
	 * POST /prospectives
	 *
	 * @return Response
	 */
	public function store()
	{
		//
	}

	/**
	 * Display the specified resource.
	 * GET /prospectives/{id}
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show($id)
	{
		$this->layout->content = View::make('admin.prospectives.show');
	}

	/**
	 * Show the form for editing the specified resource.
	 * GET /prospectives/{id}/edit
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit($id)
	{
		//
	}

	/**
	 * Update the specified resource in storage.
	 * PUT /prospectives/{id}
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update($id)
	{
		//
	}

	/**
	 * Remove the specified resource from storage.
	 * DELETE /prospectives/{id}
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($id)
	{
		//
	}

}