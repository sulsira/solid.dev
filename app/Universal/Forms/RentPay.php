<?php namespace Universal\Forms;

use Laracasts\Validation\FormValidator;

class RentPay extends FormValidator{


		/**
		 * validation rules for the plot creation form
		 * Post /plots/create
		 *
		 * @return Response 
		 */
	protected $rules = [
			  'payment_type' => 'required|max:200',
			  'payment_remark' => 'max:200',
			  'payment_house' => 'required|numeric',
			  'date_paid' => 'required',
			  'amount_paid' => 'required',
			  'number_months' => 'required',
			  'tent_id' => 'required'
	];


}