<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateLuwasTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('luwas', function(Blueprint $table)
		{
			$table->increments('id');
			$table->integer('tenant_id')->default(0);
			$table->integer('ll_id')->default(0);
			$table->integer('house_id')->default(0);
			$table->integer('compound_id')->default(0);
			$table->integer('month_num')->default(0);
			$table->string('month_short')->nullable();
			$table->string('month')->nullable();
			$table->decimal('amount',20,3)->nullable();
			$table->integer('year')->default(0);
			$table->integer('deleted')->default(0);
			$table->timestamps();
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('Luwas');
	}

}
