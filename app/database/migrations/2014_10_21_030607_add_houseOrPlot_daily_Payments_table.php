<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class AddHouseOrPlotDailyPaymentsTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('daily_transactions', function(Blueprint $table)
		{
			$table->string('house_plot_num')->nullable();
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('daily_transactions', function(Blueprint $table)
		{
			$table->dropColumn('house_plot_num');
		});
	}

}
