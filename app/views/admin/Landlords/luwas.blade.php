<?php #page specific processing
    $image = array();
    $person =  array();
    $documents =  array();
    $compounds = array();
    $house = array();
    $tenants = array();
    $rents =  array();
    $payments = array();
    $kin = array();
    if (!empty($landlord)) {
        foreach ($landlord as $key => $value) {
           if ($key == 'person') {
              $person = $value;
           }
           if ($key == 'documents') {
               foreach ($value as $d => $doc) {
                    if ($doc['type'] == 'Photo') {
                       $image = $doc;
                    }else{
                        $documents[] = $doc;
                    }
               }
           }
           if ($key == 'compounds') {

                foreach ($value as $c => $compound) {
                   $compounds[] = $compound;

                   if (isset($compound['houses'])) {
                       foreach ($compound['houses'] as $h => $house) {
                           $houses[] = $house;
                           if (isset($house['tenants'])) {
                               foreach ($house['tenants'] as $t => $tenant) {
                                   if ( $tenant['tent_status'] == 1 || $tenant['tent_status'] == 2  ) {
                                      $tenants[] = $tenant;
                                      if (isset($tenant['rents'])) {
                                         foreach ($tenant['rents'] as $r => $rent) {
                                            $rents[] = $rent;
                                            if (isset($rent['payments'])) {
                                                foreach ($rent['payments'] as $p => $payment) {
                                                $payments[] = $payment;
                                                }
                                            }

                                         }
                                      }
                                   }
                               }                               
                           }

                       }
                   }
                }

           }
           if ($key == 'kin') {
               $kin = $value;
           }
        }
    }
    $contacts = (!empty($person['contacts']))? $person['contacts'] : [];
    $addresses = (!empty($person['addresses']))? $person['addresses'] : [];

    if (!empty($person['documents'])) {
       foreach ($person['documents'] as $d => $doc) {
            if ($doc['type'] == 'Photo') {
               $image = $doc;
            }else{
                $documents[] = $doc;
            }
       }
    }
    // echo '<pre style="margin:200px 80px">';
    // var_dump($tenants);
    // print_r("<hr>");
    // echo '</pre>';
    // var_dump($landlord['rent-payment']);
    // die();
  $mons = ['1'=>'jan', '2'=>'feb', '3'=>'mar', '4'=>'apr', '5'=>'may', '6'=>'jun', '7'=>'jul', '8'=>'aug', '9'=>'sep', '10'=>'oct', '11'=>'nov', '12'=>'dec'];

 ?>
@include('templates/top-admin')
@section('content')
   <div class="scope">
        <div class="hedacont">
            <div class="navbar">
                <div class="navbar-inner" id="scopebar">
                    <div class="container">
                        <a class="btn btn-navbar" data-toggle="collapse" data-target="navbar-responsive-collapse">
                          <span class="icon-bar"></span>
                          <span class="icon-bar"></span>
                          <span class="icon-bar"></span>
                        </a>
                        <a class="brand" href="{{route('land-lords.show',$landlord['id'])}}">Land lord Name : {{ucwords( $landlord['ll_fullname'] )}}</a>
                        <div class="nav-collapse collapse navbar-responsive-collapse">
                          <ul class="nav">  
                            <li><a href="{{route('land-lords.show',$landlord['id'])}}#basic">General</a> </li>
                            <li><a href="{{route('land-lords.show',$landlord['id'])}}#compounds">Properties</a> </li>
                            <li><a href="{{route('land-lords.show',$landlord['id'])}}#houses">Houses</a> </li>
                            <li><a href="{{route('land-lords.show',$landlord['id'])}}#tenants">Tenants</a> </li>
                            <li><a href="{{route('land-lords.show',$landlord['id'])}}#documents">Documents</a></li>
                            <li>{{HTML::link('/land-lords/'.$landlord['id'].'/rents-paid','Rent-payment')}}</li> 
                            <li><a href="{{route('land-lords.show',$landlord['id'])}}#payments">Payments</a></li>
                            <li><a href="{{route('land-lords.edit',$landlord['id'])}}">Edit</a> </li>
                           </ul>
                        </div><!-- /.nav-collapse -->
                    </div>
                </div><!-- /navbar-inner -->
            </div> 
            <div class="c-header">
                <?php if (!empty($image)): ?>
                    <ul class="thumbnails" id="thmb">
                        <li class="span2">
                          <a href="#" class="thumbnail">
                           {{HTML::image($image['thumnaildir'])}}
                          </a>
                        </li>
                    </ul>                      
                <?php endif ?>

            </div>           
        </div>  
    </div>  <!-- end of scope -->
    <div class="content-details clearfix">



   <div class="cc clearfix" id="payments">
        <h3>Monthly rents </h3>
    <hr>
    <table class="table table-bordered">
      <thead>
        <tr>
            <th>Tenant</th>
            <th>Year</th>
            <th colspan="12">Months</th>
            <th colspan="3">Payments</th>
        </tr>
        <tr>
            <th></th>
            <th></th>
            <th>jan</th>
            <th>feb</th>
            <th>mar</th>
            <th>apr</th>
            <th>may</th>
            <th>jun</th>
            <th>jul</th>
            <th>aug</th>
            <th>sep</th>
            <th>oct</th>
            <th>nov</th>
            <th>dec</th>
            <th>Overpay</th>
            <th>Arrears</th>
            <th>Total</th>
        </tr>
      </thead>
      <tbody>
        <?php if (isset($landlord['rent-payment']) && !empty($landlord['rent-payment'])): ?>
          <?php foreach ($landlord['rent-payment'] as $key => $value): ?>
            <tr>

              <?php 
              $total = 0;
              $arrears = 0;
              $paid = 0;
              $balance = 0;
              $id = 0;
              $names = [];

               ?>

              <td>
                  <?php foreach ($value as $tt => $ttt): ?>
                <?php 
                $names[] = $ttt['tenant']['person']['pers_fname'].' '.$ttt['tenant']['person']['pers_mname'].' '.$ttt['tenant']['person']['pers_lname'];
                $id = $ttt['tenant_id'];
                ?>
                    
                  <?php endforeach ?>
                  <a href="{{route('tenants.show',$id)}}">{{ucwords(head($names))}}</a>
                  
              </td>
              <td>
                {{$key}}
              </td>
              <?php foreach ($mons as $index => $mth): ?>

                <td>
                <?php if (isset( $value[$mth]['month_short'] )): ?>
                   <?php if ($mth == $value[$mth]['month_short']): ?>
                     {{$value[$mth]['amount_paid']}}
                     <?php 
                        // calculating areas and others
                      if ( $value[$mth]['amount_paid'] !== $value[$mth]['amount'] ) {

                        if( $value[$mth]['amount_paid'] < $value[$mth]['amount'] ){

                          $arrears = $arrears + ($value[$mth]['amount_paid'] - $value[$mth]['amount']);

                        }
                        if( $value[$mth]['amount_paid'] > $value[$mth]['amount'] ){

                          $balance =  $balance + ($value[$mth]['amount_paid'] - $value[$mth]['amount']);

                        }


                      }

                      $total = $total + $value[$mth]['amount_paid'];
                      ?>
                    <?php else: ?>
                     <?php 
                        // calculating areas and others
                      if ( $value[$mth]['amount_paid'] !== $value[$mth]['amount'] ) {

                        if( $value[$mth]['amount_paid'] < $value[$mth]['amount'] ){

                          $arrears = $arrears + ($value[$mth]['amount_paid'] - $value[$mth]['amount']);

                        }
                        if( $value[$mth]['amount_paid'] > $value[$mth]['amount'] ){

                          $balance =  $balance + ($value[$mth]['amount_paid'] - $value[$mth]['amount']);

                        }


                      }

                      $total = $total + $value[$mth]['amount_paid'];
                      ?>                      
                   <?php endif ?>                  
                <?php endif ?>

                </td>
              <?php endforeach ?>
              <td>{{$balance}}</td>
              <td>{{$arrears}}</td>
              <td>{{$total}}</td>
            <?php endforeach ?>              

            </tr>

        <?php else: ?>
         <tr><td colspan="17"><h4>No Payment yet!</h4></td></tr>
        <?php endif ?>
      </tbody>
    </table>
   </div>

</div>


@stop
@include('templates/bottom-admin')